<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
// Module: paquet-animatecss


if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'disposition_description' => 'Rend possible la mise en page d\'un article avec des blocs responsive dans l\'espace de rédaction via Bootstrap. Complément idéal pour SPIP-R.',
	'disposition_slogan' => 'Ajoute de nouveaux modèles exploitant certaines fonctionnatliés de bootstrap : blocs responsive avec styles CSS, carousel, boutons, objets visuels. Patche bootstrap pour rendre les boites modales exploitables. Patche faiblement image_responsive.',
);
?>